:s0: anticapitalista
:s1: Linux

s0 http://www.antiracismfascism.org/ anti-fascist
s0 http://www.democracynow.org/ Democracy Now!  
s0 http://www.counterpunch.org/ CounterPunch
s0 http://www.newleftreview.org/ New Left Review 
s0 http://monthlyreview.org/ Monthly Review
s0 http://www.marxists.org/ Marxists Internet Archive
s0 http://www.chomsky.info/ The Noam Chomsky Website
s0 http://www.radicalphilosophy.com/ Radical Philosophy
s0 https://jacobinmag.com/ Jacobin
s0 https://catalyst-journal.com/ Catalyst
s1 https://www.antixforum.com/ antiX-forum 
s1 https://antixlinux.com/ antiX Website
s1 https://www.youtube.com/user/runwiththedolphin?feature=watch/ antiX-videos
s1 https://www.tapatalk.com/groups/antix/ old antiX-forum 
s1 file:/usr/share/antiX/FAQ/index.html/ antiX-FAQ
s1 https://antixlinuxfan.miraheze.org/wiki/Main_Page/ antiX wiki
s1 file:/usr/share/antiX/equivalents.html/ antiX-equivalents
